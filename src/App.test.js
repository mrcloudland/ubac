import React from 'react';
import { render } from '@testing-library/react';
import 'mutationobserver-shim';
import App from './App';

test('renders main page', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/GAME INFO/i);
  expect(linkElement).toBeInTheDocument();
});
