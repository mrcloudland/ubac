import React from 'react';
import './Footer.css';
import esrb from '../images/esrb.png';
import pc from '../images/pc.png';
import xbox from '../images/xbox.png';
import ubisoft from '../images/ubisoft.png';
import ps3 from '../images/ps3.png';
import mRating from '../images/m-rating.png';

const Footer = () => {
  return (
    <div className="Footer">
      <div className="Footer-logoItems">
        <div>
          <img src={ubisoft} alt="" />
          <img src={xbox} alt="" />
          <img src={ps3} alt="" />
          <img src={pc} alt="" />
        </div>
        <div>
          <img src={esrb} alt="" />
          <img src={mRating} alt="" />
        </div>
      </div>
      <div className="Footer-disclaimer">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget justo et lorem iaculis ullamcorper. Vivamus dignissim dapibus felis ac rhoncus. Nulla facilisi. Morbi tincidunt in massa eu iaculis. Ut non magna porttitor, hendrerit turpis non, venenatis felis. Maecenas blandit fermentum tempus. Sed ut neque auctor odio lobortis vulputate. Morbi commodo justo fringilla, euismod nunc ac, facilisis nunc.
      </div>
    </div>
  );
};

export default Footer;
