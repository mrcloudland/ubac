import React, { useState } from 'react';
import Carousel from 'nuka-carousel';
import './Promo.css';
import promo1 from '../images/promo-1.png';
import promo2 from '../images/promo-2.png';
import promo3 from '../images/promo-3.png';
import promoThumb1 from '../images/promo-thumb-1.png';
import promoThumb2 from '../images/promo-thumb-2.png';
import promoThumb3 from '../images/promo-thumb-3.png';

const Promo = () => {
  const [slideIndex, setSlideIndex] = useState(0);
  return (
    <div className="Promo">
        <Carousel
          autoplay
          pauseOnHover
          withoutControls
          transitionMode="fade"
          slideIndex={slideIndex}
          afterSlide={newSlideIndex => setSlideIndex(newSlideIndex)}
        >
            <img src={promo1} alt="promo1" />
            <img src={promo2} alt="promo2" />
            <img src={promo3} alt="promo3" />
        </Carousel>
        <div className="Promo-thumbnailBar">
          <div
            className={`Promo-thumbnailBar-box ${ slideIndex === 0 ? 'Promo-thumbnailBar-box-selected' : ''}`}
            onClick={() => {setSlideIndex(0)}}
          >
            <img
              className="Promo-thumbnailBar-thumbnail"
              src={promoThumb1}
              alt="promo-thumbnail-1"
            />
            <div className="Promo-thumbnailBar-details">
              <div className="Promo-thumbnailBar-title">ASSASSIN CREED</div>
              <div className="Promo-thumbnailBar-desc">The truth is written in blood, all is forgetten but never seen. Assasins will come to those disrespect our laws.</div>
            </div>
          </div>

          <div
            className={`Promo-thumbnailBar-box ${ slideIndex === 1 ? 'Promo-thumbnailBar-box-selected' : ''}`}
            onClick={() => {setSlideIndex(1)}}
          >
            <img
              className="Promo-thumbnailBar-thumbnail"
              src={promoThumb2}
              alt="promo-thumbnail-2"
            />
            <div className="Promo-thumbnailBar-details">
              <div className="Promo-thumbnailBar-title">ASSASSIN CREED</div>
              <div className="Promo-thumbnailBar-desc">The truth is written in blood, all is forgetten but never seen. Assasins will come to those disrespect our laws.</div>
            </div>
          </div>

          <div
            className={`Promo-thumbnailBar-box ${ slideIndex === 2 ? 'Promo-thumbnailBar-box-selected' : ''}`}
            onClick={() => {setSlideIndex(2)}}
          >
            <img
              className="Promo-thumbnailBar-thumbnail"
              src={promoThumb3}
              alt="promo-thumbnail-3"
            />
            <div className="Promo-thumbnailBar-details">
              <div className="Promo-thumbnailBar-title">ASSASSIN CREED</div>
              <div className="Promo-thumbnailBar-desc">The truth is written in blood, all is forgetten but never seen. Assasins will come to those disrespect our laws.</div>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Promo;
