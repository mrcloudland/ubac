import React from 'react';
import './Header.css';
import ad from '../images/ad.png';

const Header = () => {
  return (
    <header className="Header">
      <a href="/">
        <div className="Header-logo-image"></div>
      </a>
      <img src={ad} alt="ad" />
    </header>
  );
};

export default Header;
