import React, { useState, useEffect } from 'react';
import ModalVideo from 'react-modal-video'
import 'react-modal-video/css/modal-video.min.css';
import './Videos.css';
import promoThumb1 from '../images/promo-thumb-1.png';

const videoListData = [
  {
    title: 'ASSASSIN CREED',
    desc: 'The truth is written in blood, all is forgetten but never seen. Assasins will come to those disrespect our laws.',
    id: 'E7vBgW6mP9I',
    isOpen: false,
  },
  {
    title: 'ASSASSIN CREED',
    desc: 'The truth is written in blood, all is forgetten but never seen. Assasins will come to those disrespect our laws.',
    id: 'XZYS_nFiWsI',
    isOpen: false,
  },
  {
    title: 'ASSASSIN CREED',
    desc: 'The truth is written in blood, all is forgetten but never seen. Assasins will come to those disrespect our laws.',
    id: 'gZrklEy9ohQ',
    isOpen: false,
  },
];

const Videos = () => {
  const [videoItems, setVideoItems] = useState([]);
  useEffect(() => {
    setVideoItems(videoListData);
  }, []);
  return (
    <div className="Videos">
      <div className="Videos-titleBar">LIFE OF AN ASSASSIN</div>
      <div className="Videos-videoItems">
        {videoItems.map((videoItem, index) => (
          <div key={videoItem.title + index}>
            <div
              className="Videos-videoItem-box"
              onClick={() => {
                const newVideoItems = [...videoItems];
                newVideoItems[index].isOpen = true;
                setVideoItems(newVideoItems);
              }}
            >
              <img
                className="Videos-videoItem-thumbnail"
                src={promoThumb1}
                alt="promo-thumbnail-1"
              />
              <div className="Videos-videoItem-details">
                <div className="Videos-videoItem-title">{videoItem.title}</div>
                <div className="Videos-videoItem-desc">{videoItem.desc}</div>
              </div>
            </div>
            <ModalVideo
              channel='youtube'
              isOpen={videoItem.isOpen}
              videoId={videoItem.id}
              onClose={() => {
                const newVideoItems = [...videoItems];
                newVideoItems[index].isOpen = false;
                setVideoItems(newVideoItems);
              }}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Videos;
