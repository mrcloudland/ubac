import React from 'react';
import { Link } from 'react-router-dom';
import './Nav.css';

const Nav = () => {
  return (
    <nav className="Nav">
      <Link className="Nav-link" to="/info">GAME INFO</Link>
      <Link className="Nav-link" to="/universe">ASSASSINS CREED UNIVERSE</Link>
      <Link className="Nav-link" to="/uplay">UPLAY</Link>
      <Link className="Nav-link" to="/forums">FORUMS</Link>
      <Link className="Nav-link" to="/register">REGISTER FOR UPDATES</Link>
      <Link className="Nav-link" to="/pre-order">PRE-ORDER</Link>
    </nav>
  );
};

export default Nav;
