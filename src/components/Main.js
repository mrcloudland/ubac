import React from 'react';
import Promo from './Promo';
import Videos from './Videos';
import News from './News';
import './Main.css';

const Main = () => {
  return (
    <div>
      <Promo />
      <div className="Main-content">
        <Videos />
        <News />
      </div>
    </div>
  );
};

export default Main;
