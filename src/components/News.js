import React, { useState, useEffect } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import './News.css';
import promoThumb1 from '../images/promo-thumb-1.png';

const newsData = {
  image: promoThumb1,
  title: 'ASSASSIN CREED',
  desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget justo et lorem iaculis ullamcorper. Vivamus dignissim dapibus felis ac rhoncus. Nulla facilisi. Morbi tincidunt in massa eu iaculis. Ut non magna porttitor, hendrerit turpis non, venenatis felis. Maecenas blandit fermentum tempus. Sed ut neque auctor odio lobortis vulputate. Morbi commodo justo fringilla, euismod nunc ac, facilisis nunc.',
};

const News = () => {
  const [newsItems, setNewsItems] = useState([]);
  useEffect(() => {
    setNewsItems([newsData, newsData, newsData, newsData]);
  }, []);
  const loadMore = () => {
    setTimeout(() => {
      setNewsItems([...newsItems, newsData, newsData]);
    }, 2000);
  };
  return (
    <div className="News">
      <div className="News-titleBar">LIFE OF AN ASSASSIN</div>
      <div className="News-newsItems">
        <InfiniteScroll
            loadMore={loadMore}
            hasMore={newsItems.length < 12}
            loader={<div key={0}>Loading ...</div>}
            useWindow={false}
        >
          {newsItems.map((item, index) => (
            <div className="News-newsItem-box" key={item.title + index}>
              <img
                className="News-newsItem-thumbnail"
                src={item.image}
                alt="promo-thumbnail-1"
              />
              <div className="News-newsItem-details">
                <div className="News-newsItem-title">{item.title}</div>
                <div className="News-newsItem-desc">{item.desc}</div>
              </div>
            </div>
          ))}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default News;
