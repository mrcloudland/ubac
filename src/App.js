import React from 'react';
import {
  HashRouter as Router, Route, Switch
} from 'react-router-dom';
import 'normalize.css';
import './App.css';
import Header from './components/Header';
import Nav from './components/Nav';
import Footer from './components/Footer';
import Main from './components/Main';
import NotFound from './components/NotFound';

const App = () => {
  return (
    <div className="App">
      <div className="App-content">
        <Header />
        <Router>
          <Nav />
          <Switch>
            <Route exact path="/" component={Main} />
            <Route component={NotFound} />
          </Switch>
        </Router>
        <Footer />
      </div>
    </div>
  );
};

export default App;
